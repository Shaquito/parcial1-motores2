using UnityEngine;

public class DoorButton : MonoBehaviour
{
    public GameObject doorPrefab; // Prefab de la puerta asignado desde el Inspector
    public float moveDistance = 5f; // Distancia que se movera la puerta
    public float moveSpeed = 2f; // Velocidad a la que se movera la puerta

    private Vector3 initialPosition;
    private Vector3 targetPosition;
    private static int buttonsPressed = 0; // Variable estatica para contar botones presionados

    private void Start()
    {
        if (doorPrefab != null)
        {
            initialPosition = doorPrefab.transform.position;
            targetPosition = initialPosition + new Vector3(0, moveDistance, 0);
        }
    }

    private void Update()
    {
        if (buttonsPressed > 0)
        {
            MoveDoor(targetPosition);
        }
        else
        {
            MoveDoor(initialPosition);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            buttonsPressed++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            buttonsPressed--;
        }
    }

    private void MoveDoor(Vector3 target)
    {
        if (doorPrefab != null)
        {
            doorPrefab.transform.position = Vector3.MoveTowards(doorPrefab.transform.position, target, moveSpeed * Time.deltaTime);
        }
    }
}
