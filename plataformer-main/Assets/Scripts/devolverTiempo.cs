using UnityEngine;

public class devolverTiempo : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Time.timeScale = 1f;

            Destroy(gameObject);

        }
        
    }

}
