using UnityEngine;

public class GenerarPlataformas : MonoBehaviour
{
    public GameObject platformPrefab; // Prefab de la plataforma desde el Inspector
    public float groundDistance = 0.1f; // Distancia para el raycast desde el jugador hacia abajo
    public LayerMask groundLayer; // Capa del suelo

    private void Update()
    {
        bool isGrounded = Physics.Raycast(transform.position, Vector3.down, groundDistance, groundLayer);

        if (Input.GetKeyDown(KeyCode.Space) && !isGrounded)
        {
            SpawnPlatform();
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            DestroyLastPlatform();
        }
    }

    private void SpawnPlatform()
    {
        Vector3 spawnPosition = transform.position + Vector3.down * 1.5f; // Posicion abajo del jugador.
        Instantiate(platformPrefab, spawnPosition, Quaternion.identity);
    }

    private void DestroyLastPlatform()
    {
        GameObject[] platforms = GameObject.FindGameObjectsWithTag("Platform");

        if (platforms.Length > 0)
        {
            GameObject lastPlatform = platforms[platforms.Length - 1];
            Destroy(lastPlatform);
        }
    }
}
