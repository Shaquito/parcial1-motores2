using UnityEngine;
using System.Collections;

public class powerUpTiempo : MonoBehaviour
{
    public float slowDuration = 5f;  // Duración del efecto de ralentización
    public float slowFactor = 0.5f;  // Factor de ralentización (0.5 significa la mitad de la velocidad normal)

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Time.timeScale = slowFactor;
            Destroy(gameObject);
        }
    }
}