using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform startPoint; // Punto de inicio de la plataforma
    public Transform endPoint; // Punto final de la plataforma
    public float speed = 2f; // Velocidad de movimiento de la plataforma
    public float delayTime = 1f; // Tiempo de espera en cada extremo

    private Vector3 targetPosition;
    private bool movingToEnd = true;
    private float waitTime;

    void Start()
    {
        targetPosition = endPoint.position;
        waitTime = delayTime;
    }

    void Update()
    {
        // Mover la plataforma hacia el punto final o de regreso al punto de inicio
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        // Si la plataforma alcanza uno de los puntos, cambiar la direccion y esperar un tiempo
        if (transform.position == targetPosition)
        {
            if (movingToEnd)
            {
                targetPosition = startPoint.position;
            }
            else
            {
                targetPosition = endPoint.position;
            }
            movingToEnd = !movingToEnd;
            waitTime = delayTime;
        }

        // Esperar un tiempo en cada extremo
        if (waitTime > 0)
        {
            waitTime -= Time.deltaTime;
        }
    }
}
